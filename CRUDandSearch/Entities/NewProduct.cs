﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CRUDandSearch.Entities
{
    [Table("NewProduct")]
    public class NewProduct
    {
        public int Id { get; set; }
        public string ProdName { get; set; }
        public decimal ProdPrice { get; set; }
        public int Count { get; set; }
        public bool DeletedStatus { get; set; }
    }
}
