﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CRUDandSearch.Entities
{
    public class MarketContext:DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder dbContextOptionsBuilder)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
            var configur = builder.Build();
            dbContextOptionsBuilder.UseSqlServer(configur["ConnectionStrings:MarketContext"]);
        }
        public DbSet<NewProduct> NewProducts { get; set; }
    }
}
