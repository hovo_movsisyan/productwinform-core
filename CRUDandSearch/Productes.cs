﻿using CRUDandSearch.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace CRUDandSearch
{
    public partial class Productes : Form
    {
        private readonly MarketContext db = new MarketContext();
        public Productes()
        {
            InitializeComponent();
        }

        private void Productes_Load(object sender, EventArgs e)
        {
            dataGridViewProducts.DataSource = db.NewProducts.Where(m => m.DeletedStatus == false).ToList();
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            try
            {
                var product = new NewProduct
                {
                    ProdName = textBoxForProductName.Text,
                    ProdPrice = decimal.Parse(textBoxForProductPrice.Text),
                    Count = int.Parse(textBoxForProductCount.Text),
                };
                db.NewProducts.Add(product);
                db.SaveChanges();
                MessageBox.Show("Success!!!");
                dataGridViewProducts.DataSource = db.NewProducts.Where(m => m.DeletedStatus == false).ToList();
            }
            catch
            {
                MessageBox.Show("Try Again");
            }
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                var product = db.NewProducts.Find(int.Parse(textBoxForId.Text));
                product.ProdName = textBoxForProductName.Text;
                product.ProdPrice = decimal.Parse(textBoxForProductPrice.Text);
                product.Count = int.Parse(textBoxForProductCount.Text);
                db.SaveChanges();
                MessageBox.Show("Success!!!");
                dataGridViewProducts.DataSource = db.NewProducts.Where(m => m.DeletedStatus == false).ToList();
            }
            catch 
            {
                MessageBox.Show("Try Again");
            }
        }

        private void ButtonRemove_Click(object sender, EventArgs e)
        {
            try
            {
                var messege = MessageBox.Show("Do you want to Remove?", "Confirm",
                    MessageBoxButtons.YesNo);
                if (messege == DialogResult.Yes)
                {
                    var id = int.Parse(textBoxForId.Text);
                    var product = db.NewProducts.Find(id);
                    product.DeletedStatus = true;
                    db.SaveChanges();
                    MessageBox.Show("Success!!!");
                    dataGridViewProducts.DataSource = db.NewProducts.Where(m => m.DeletedStatus == false).ToList();
                }
            }
            catch
            {
                MessageBox.Show("Try Again");
            }
        }

        private void DataGridViewProducts_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                var index = int.Parse(dataGridViewProducts.Rows[e.RowIndex].Cells[0].Value.ToString());
                var product = db.NewProducts.Find(index);
                textBoxForId.Text = product.Id.ToString();
                textBoxForProductName.Text = product.ProdName;
                textBoxForProductCount.Text = product.Count.ToString();
                textBoxForProductPrice.Text = product.ProdPrice.ToString();
                this.buttonUpdate.Enabled = true;
                this.buttonRemove.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Correct your choice");
            }

        }
        private void TextBoxForProductSearch_TextChanged(object sender, EventArgs e)
        {
            dataGridViewProducts.DataSource = db.NewProducts.Where
                          (x => x.ProdName.StartsWith(textBoxForProductSearch.Text) && 
                                x.DeletedStatus==false).ToList();
        }

    }
}
