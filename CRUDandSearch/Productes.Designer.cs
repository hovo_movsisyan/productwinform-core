﻿using System.Windows.Forms;

namespace CRUDandSearch
{
    partial class Productes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewProducts = new System.Windows.Forms.DataGridView();
            this.panelProduct = new System.Windows.Forms.Panel();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.textBoxForId = new System.Windows.Forms.TextBox();
            this.textBoxForProductPrice = new System.Windows.Forms.TextBox();
            this.textBoxForProductCount = new System.Windows.Forms.TextBox();
            this.textBoxForProductName = new System.Windows.Forms.TextBox();
            this.textBoxForProductSearch = new System.Windows.Forms.TextBox();
            this.labelSearchProduct = new System.Windows.Forms.Label();
            this.labelProductPrice = new System.Windows.Forms.Label();
            this.labelProductCount = new System.Windows.Forms.Label();
            this.labelProductName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProducts)).BeginInit();
            this.panelProduct.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewProducts
            // 
            this.dataGridViewProducts.BackgroundColor = System.Drawing.Color.White;
            this.dataGridViewProducts.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridViewProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProducts.Location = new System.Drawing.Point(12, 12);
            this.dataGridViewProducts.Name = "dataGridViewProducts";
            this.dataGridViewProducts.Size = new System.Drawing.Size(370, 172);
            this.dataGridViewProducts.TabIndex = 0;
            this.dataGridViewProducts.MultiSelect = false;
            this.dataGridViewProducts.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewProducts_CellContentClick);
            // 
            // panelProduct
            // 
            this.panelProduct.Controls.Add(this.buttonRemove);
            this.panelProduct.Controls.Add(this.buttonUpdate);
            this.panelProduct.Controls.Add(this.buttonSave);
            this.panelProduct.Controls.Add(this.textBoxForProductPrice);
            this.panelProduct.Controls.Add(this.textBoxForProductCount);
            this.panelProduct.Controls.Add(this.textBoxForProductName);
            this.panelProduct.Controls.Add(this.textBoxForProductSearch);
            this.panelProduct.Controls.Add(this.textBoxForId);
            this.panelProduct.Controls.Add(this.labelSearchProduct);
            this.panelProduct.Controls.Add(this.labelProductPrice);
            this.panelProduct.Controls.Add(this.labelProductCount);
            this.panelProduct.Controls.Add(this.labelProductName);
            this.panelProduct.Location = new System.Drawing.Point(12, 190);
            this.panelProduct.Name = "panelProduct";
            this.panelProduct.Size = new System.Drawing.Size(370, 186);
            this.panelProduct.TabIndex = 1;

            // 
            // buttonRemove
            // 
            this.buttonRemove.Location = new System.Drawing.Point(230, 153);
            this.buttonRemove.Name = "buttonRemove";
            this.buttonRemove.Size = new System.Drawing.Size(56, 23);
            this.buttonRemove.TabIndex = 11;
            this.buttonRemove.Text = "Remove";
            this.buttonRemove.Enabled = false;
            this.buttonRemove.BackColor = System.Drawing.Color.Red;
            this.buttonRemove.Click += new System.EventHandler(this.ButtonRemove_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(169, 153);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(56, 23);
            this.buttonUpdate.TabIndex = 10;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.Enabled = false;
            this.buttonUpdate.BackColor = System.Drawing.Color.Orange;
            this.buttonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(107, 153);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(56, 23);
            this.buttonSave.TabIndex = 9;
            this.buttonSave.Text = "Save";
            this.buttonSave.BackColor = System.Drawing.Color.Green;
            this.buttonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // textBoxForId
            // 
            this.textBoxForProductPrice.Name = "textBoxForId";
            this.textBoxForId.Visible = false;
            // 
            // textBoxForProductSearch
            // 
            this.textBoxForProductSearch.Location = new System.Drawing.Point(35, 15);
            this.textBoxForProductSearch.Name = "textBoxForProductSearch";
            this.textBoxForProductSearch.Size = new System.Drawing.Size(321, 20);
            this.textBoxForProductSearch.TabIndex = 12;
            this.textBoxForProductSearch.TextChanged += new System.EventHandler(this.TextBoxForProductSearch_TextChanged);
            // 
            // textBoxForProductPrice
            // 
            this.textBoxForProductPrice.Location = new System.Drawing.Point(107, 105);
            this.textBoxForProductPrice.Name = "textBoxForProductPrice";
            this.textBoxForProductPrice.Size = new System.Drawing.Size(179, 20);
            this.textBoxForProductPrice.TabIndex = 7;
            // 
            // textBoxForProductCount
            // 
            this.textBoxForProductCount.Location = new System.Drawing.Point(107, 79);
            this.textBoxForProductCount.Name = "textBoxForProductCount";
            this.textBoxForProductCount.Size = new System.Drawing.Size(179, 20);
            this.textBoxForProductCount.TabIndex = 6;
            // 
            // textBoxForProductName
            // 
            this.textBoxForProductName.Location = new System.Drawing.Point(107, 53);
            this.textBoxForProductName.Name = "textBoxForProductName";
            this.textBoxForProductName.Size = new System.Drawing.Size(179, 20);
            this.textBoxForProductName.TabIndex = 5;
            // 
            // Product Price
            // 
            this.labelProductPrice.AutoSize = true;
            this.labelProductPrice.ForeColor = System.Drawing.SystemColors.Control;
            this.labelProductPrice.Location = new System.Drawing.Point(19, 112);
            this.labelProductPrice.Name = "labelProductPrice";
            this.labelProductPrice.Size = new System.Drawing.Size(71, 13);
            this.labelProductPrice.TabIndex = 3;
            this.labelProductPrice.Text = "Product Price";
            // 
            // Search Product
            // 
            this.labelSearchProduct.AutoSize = true;
            this.labelSearchProduct.ForeColor = System.Drawing.SystemColors.Control;
            this.labelSearchProduct.Location = new System.Drawing.Point(155, 2);
            this.labelSearchProduct.Name = "labelSearchProduct";
            this.labelSearchProduct.Size = new System.Drawing.Size(81, 13);
            this.labelSearchProduct.TabIndex = 13;
            this.labelSearchProduct.Text = "Search Product";
            // 
            // Product Count
            // 
            this.labelProductCount.AutoSize = true;
            this.labelProductCount.ForeColor = System.Drawing.SystemColors.Control;
            this.labelProductCount.Location = new System.Drawing.Point(19, 82);
            this.labelProductCount.Name = "labelProductCount";
            this.labelProductCount.Size = new System.Drawing.Size(75, 13);
            this.labelProductCount.TabIndex = 2;
            this.labelProductCount.Text = "Product Count";
            // 
            // ProductName
            // 
            this.labelProductName.AutoSize = true;
            this.labelProductName.ForeColor = System.Drawing.SystemColors.Control;
            this.labelProductName.Location = new System.Drawing.Point(19, 55);
            this.labelProductName.Name = "labelProductName";
            this.labelProductName.Size = new System.Drawing.Size(75, 13);
            this.labelProductName.TabIndex = 1;
            this.labelProductName.Text = "Product Name";
            // 
            // Productes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(450, 383);
            this.Controls.Add(this.panelProduct);
            this.Controls.Add(this.dataGridViewProducts);
            this.Name = "Productes";
            this.Text = "Products";
            this.Load += new System.EventHandler(this.Productes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProducts)).EndInit();
            this.panelProduct.ResumeLayout(false);
            this.panelProduct.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.DataGridView dataGridViewProducts;
        private System.Windows.Forms.Panel panelProduct;
        private System.Windows.Forms.Label labelProductName;
        private System.Windows.Forms.Label labelSearchProduct;
        private System.Windows.Forms.Label labelProductPrice;
        private System.Windows.Forms.Label labelProductCount;
        private System.Windows.Forms.TextBox textBoxForProductPrice;
        private System.Windows.Forms.TextBox textBoxForProductCount;
        private System.Windows.Forms.TextBox textBoxForProductName;
        private System.Windows.Forms.TextBox textBoxForProductSearch;
        private System.Windows.Forms.TextBox textBoxForId;
        private System.Windows.Forms.Button buttonRemove;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Button buttonSave;
    }
}